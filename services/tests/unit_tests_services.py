from django.http import HttpRequest, HttpResponse, JsonResponse
from django.test import TestCase
from mock import mock, MagicMock
from rest_framework import status
from rest_framework.utils import json

from services.daily_horoscope import get_daily_horoscope_for, requests


class ServicesUnitTests(TestCase):
    def test_get_daily_horoscope_for(self):
        req = mock.Mock(spec=HttpRequest)
        req.user = MagicMock(return_value="")
        req.user.sign = MagicMock(return_value="")
        req.user.sign.capitalize = MagicMock(return_value="test")
        resp = mock.Mock
        resp.json = MagicMock(return_value={'description': "desr"})
        resp.status_code = 200
        requests.request = MagicMock(return_value=resp)
        r = get_daily_horoscope_for(req)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.content, json.dumps({"horoscope": "desr"}).encode('utf-8'))