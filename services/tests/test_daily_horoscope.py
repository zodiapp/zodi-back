import datetime
from django.test import TestCase, RequestFactory
from django.urls import reverse
from datetime import datetime

from users.models import ZodiUser


class HoroscopeTestCase(TestCase):
    USERNAME1 = "oleg"
    PASSWORD = "some_pass"

    def setUp(self):
        self.user1 = ZodiUser.objects.create_user(
            username=self.USERNAME1, password=self.PASSWORD,
            birthdate=datetime.strptime('01.01.1999', '%d.%m.%Y')  # CAPRICORN
        )

    def test_get_daily_horoscope_non_auth(self):
        response = self.client.get(
            path=reverse('index'),
        )
        self.assertEqual(response.status_code, 302)

    def test_get_daily_horoscope_for_user(self):
        self.client.login(username=self.USERNAME1, password=self.PASSWORD)
        response = self.client.get(
            path=reverse('index'),
        )
        self.assertEqual(response.status_code, 200)
        horoscope = response.json()

        self.assertIn("horoscope", horoscope)
