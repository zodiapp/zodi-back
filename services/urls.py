from django.urls import path, include

from . import daily_horoscope

urlpatterns = [
    path('', daily_horoscope.get_daily_horoscope_for, name='index'),
]
