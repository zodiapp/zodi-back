import requests
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponse

url = "https://sameer-kumar-aztro-v1.p.rapidapi.com/"

headers = {
    'x-rapidapi-key': "29cc7723cemsh7bf8f4b70cb12f6p1ef2b8jsnb25463663024",
    'x-rapidapi-host': "sameer-kumar-aztro-v1.p.rapidapi.com"
}

@login_required
def get_daily_horoscope_for(request):
    sign = request.user.sign.capitalize()
    querystring = {"sign": sign, "day": "today"}
    response = requests.request("POST", url, headers=headers, params=querystring)
    if response.status_code != 200:
        return HttpResponse(500, "Something went wrong")

    response = {"horoscope": response.json()['description']}

    return JsonResponse(data=response)
