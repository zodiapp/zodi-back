from .views import get_list, UserViewSet
from django.urls import path, include
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'people', UserViewSet, basename='search')

urlpatterns = [
    path('', include(router.urls)),
]
