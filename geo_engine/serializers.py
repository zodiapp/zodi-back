from rest_framework import serializers

from users.models import ZodiUser


class UserSearchSerializer(serializers.HyperlinkedModelSerializer):

    distance = serializers.SerializerMethodField()
    match_type = serializers.SerializerMethodField()

    def get_distance(self, obj):
        request_user = self.context['request'].user
        return obj.count_distance(request_user.latitude, request_user.longitude)

    def get_match_type(self, obj):
        request_user = self.context['request'].user
        return obj.type_math(request_user.birthdate)

    class Meta:
        model = ZodiUser
        fields = ['username', 'sex', 'birthdate', 'sign', 'distance', 'match_type']
