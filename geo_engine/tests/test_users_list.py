import json
from datetime import datetime

from django.test import TestCase, RequestFactory
from django.urls import reverse

from users.models import ZodiUser


class UserListTestCase(TestCase):
    USERNAME1 = "oleg"
    USERNAME2 = "olga"
    USERNAME3 = "georg"
    USERNAME4 = "anya"
    USERNAME5 = "lyosha"
    USERNAME6 = "sergey"
    PASSWORD = "some_pass"

    def setUp(self):
        self.user1 = ZodiUser.objects.create_user(
            username=self.USERNAME1, password=self.PASSWORD, sex=ZodiUser.Sex.MAN,
            birthdate=datetime.strptime('01.01.1999', '%d.%m.%Y'),  # CAPRICORN
            preference_relationship=ZodiUser.PreferenceRelationship.SEX,
            preference_gender=ZodiUser.PreferenceGender.BOTH,
            latitude=52.123123,
            longitude=32.122313
        )
        self.user2 = ZodiUser.objects.create_user(
            username=self.USERNAME2, password=self.PASSWORD, sex=ZodiUser.Sex.WOMAN,
            birthdate=datetime.strptime('30.06.1989', '%d.%m.%Y'),  # CANCER
            preference_relationship=ZodiUser.PreferenceRelationship.FRIEND,
            preference_gender=ZodiUser.PreferenceGender.MALE,
            latitude=53.123123,
            longitude=32.122313
        )
        self.user3 = ZodiUser.objects.create_user(
            username=self.USERNAME3, password=self.PASSWORD, sex=ZodiUser.Sex.MAN,
            birthdate=datetime.strptime('29.04.2000', '%d.%m.%Y'),  # TAURUS
            preference_relationship=ZodiUser.PreferenceRelationship.FRIEND,
            preference_gender=ZodiUser.PreferenceGender.FEMALE,
            latitude=52.123123,
            longitude=33.122313
        )
        self.user4 = ZodiUser.objects.create_user(
            username=self.USERNAME4, password=self.PASSWORD, sex=ZodiUser.Sex.WOMAN,
            birthdate=datetime.strptime('04.07.1999', '%d.%m.%Y'),  # CANCER
            preference_relationship=ZodiUser.PreferenceRelationship.SEX,
            preference_zodiac=[ZodiUser.Sign.AQUARIUS],
            preference_gender=ZodiUser.PreferenceGender.MALE,
            latitude=54.123123,
            longitude=32.122313
        )
        self.user5 = ZodiUser.objects.create_user(
            username=self.USERNAME5, password=self.PASSWORD, sex=ZodiUser.Sex.MAN,
            birthdate=datetime.strptime('07.01.2000', '%d.%m.%Y'),  # CAPRICORN
            preference_relationship=ZodiUser.PreferenceRelationship.SEX,
            preference_gender=ZodiUser.PreferenceGender.FEMALE,
            latitude=52.323123,
            longitude=32.122313
        )
        self.user6 = ZodiUser.objects.create_user(
            username=self.USERNAME6, password=self.PASSWORD, sex=ZodiUser.Sex.MAN,
            birthdate=datetime.strptime('29.04.2000', '%d.%m.%Y'),  # TAURUS
            preference_zodiac=[ZodiUser.Sign.TAURUS],
            preference_relationship=ZodiUser.PreferenceRelationship.FRIEND,
            preference_gender=ZodiUser.PreferenceGender.MALE,
            latitude=52.123999,
            longitude=32.122313
        )

    def test_get_list_non_auth(self):
        response = self.client.get(
            path=reverse('search-list'),
        )
        self.assertEqual(response.status_code, 401)

    def test_get_list_with_both(self):
        self.client.login(username=self.USERNAME1, password=self.PASSWORD)
        response = self.client.get(
            path=reverse('search-list'),
        )
        self.assertEqual(response.status_code, 200)
        people = response.json()
        self.assertEqual(len(people), 2)
        self.assertEqual(people[0]["username"], self.USERNAME4)
        self.assertEqual(people[1]["username"], self.USERNAME5)

    def test_get_list(self):
        self.client.login(username=self.USERNAME3, password=self.PASSWORD)
        response = self.client.get(
            path=reverse('search-list'),
        )
        self.assertEqual(response.status_code, 200)
        people = response.json()
        self.assertEqual(len(people), 1)
        self.assertEqual(people[0]["username"], self.USERNAME2)

    def test_get_list_with_pref_sign(self):
        self.client.login(username=self.USERNAME6, password=self.PASSWORD)
        response = self.client.get(
            path=reverse('search-list'),
        )
        self.assertEqual(response.status_code, 200)
        people = response.json()
        self.assertEqual(len(people), 1)
        self.assertEqual(people[0]["username"], self.USERNAME3)
