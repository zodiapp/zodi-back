from django.test import TestCase

from users.models import ZodiUser
from datetime import datetime


class SignMathcingTestCase(TestCase):
    USERNAME1 = "lyosha"
    USERNAME2 = "sergey"
    PASSWORD = "some_pass"

    def setUp(self):
        self.user1 = ZodiUser.objects.create_user(
            username=self.USERNAME1, password=self.PASSWORD,
            birthdate=datetime.strptime('27.02.1989', '%d.%m.%Y')
        )
        self.user2 = ZodiUser.objects.create_user(
            username=self.USERNAME2, password=self.PASSWORD,
            birthdate=datetime.strptime('21.05.1987', '%d.%m.%Y')
        )

    def test_got_result(self):
        """Two correct dates are given and mathing result - returned"""
        mathcing_result = self.user1.type_math(self.user2.birthdate)
        self.assertTrue(len(mathcing_result))
