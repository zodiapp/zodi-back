from django.apps import AppConfig


class GeoEngineConfig(AppConfig):
    name = 'geo_engine'
