from django.contrib.auth.decorators import login_required
from django.db.models import Q
from rest_framework import viewsets, permissions

from geo_engine.serializers import UserSearchSerializer
from users.models import ZodiUser


class UserViewSet(viewsets.ModelViewSet):
    queryset = ZodiUser.objects.all()
    serializer_class = UserSearchSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        return get_list(self.request)


def get_list(request):
    user: ZodiUser = request.user

    accepted = ZodiUser.objects.filter(
        Q(preference_relationship=user.preference_relationship)
    )

    if user.preference_gender != ZodiUser.PreferenceGender.BOTH:
        accepted = accepted.filter(sex=user.preference_gender)

    accepted = accepted.exclude(id=user.id)

    if user.preference_zodiac:
        accepted = accepted.filter(sign__in=user.preference_zodiac)

    #accepted = accepted.filter(Q(preference_zodiac=None) | Q(preference_zodiac__has_key=user.sign))

    # TODO: remove blocked users (after matches)

    # TODO: sort distance

    return accepted
