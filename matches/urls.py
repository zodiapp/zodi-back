from django.urls import path, include

import matches.views

urlpatterns = [
    path('match/', matches.views.MatchView.match, name='match'),
    path('block/', matches.views.MatchView.block, name='block'),
]