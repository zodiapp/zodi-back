import json

from django.http import HttpResponse
from django.shortcuts import render
from django.http import JsonResponse
# Create your views here.
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.response import Response

from chats.models import Dialog
from matches.models import Match
from users.models import ZodiUser


class MatchView(View):
    @csrf_exempt
    def match(request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        user1 = get_object_or_404(ZodiUser, id=body['user1'])
        user2 = get_object_or_404(ZodiUser, id=body['user2'])
        if Match.is_blocked(user=user2, partner=user1):
            if Match.is_blocked(user=user1, partner=user2):
                return JsonResponse(status=status.HTTP_200_OK, data={"status": "already_blocked"})
            else:
                match_pair = Match.objects.create(user=user1, partner=user2, status=Match.Status.BLOCKED)
                match_pair.save()
                return JsonResponse(status=status.HTTP_201_CREATED, data={"status": "blocked"})
        else:
            if Match.already_matched(user=user1, partner=user2):
                return JsonResponse(status=status.HTTP_200_OK, data={"status": "already_matched"})
            match_pair = Match.objects.create(user=user1, partner=user2, status=Match.Status.MATCHED)
            match_pair.save()
            if Match.partner_matched(user=user1, partner=user2):
                dialog = Dialog.objects.create(user1=user1, user2=user2, last_message="New Match!")
                dialog.save()
                return JsonResponse(status=status.HTTP_201_CREATED, data={"status": "matched"})
            return JsonResponse(status=status.HTTP_201_CREATED, data={"status": "match"})

    @csrf_exempt
    def block(request):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        user1 = get_object_or_404(ZodiUser, id=body['user1'])
        user2 = get_object_or_404(ZodiUser, id=body['user2'])

        if Match.is_blocked(user=user1, partner=user2):
            return JsonResponse(status=status.HTTP_200_OK, data={"status": "already_blocked"})
        else:
            match_pair = Match.objects.create(user=user1, partner=user2, status=Match.Status.BLOCKED)
            match_pair.save()
            return JsonResponse(status=status.HTTP_201_CREATED, data={"status": "blocked"})
