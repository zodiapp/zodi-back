from django.db import models

# Create your models here.
from users.models import ZodiUser


class Match(models.Model):
    class Status(models.TextChoices):
        BLOCKED = 'BLOCKED'
        MATCHED = 'MATCHED'

    user = models.ForeignKey(ZodiUser, related_name='user', on_delete=models.CASCADE)
    partner = models.ForeignKey(ZodiUser, related_name='partner', on_delete=models.CASCADE)
    status = models.CharField(max_length=255, choices=Status.choices,
                              null=True, blank=True)

    @staticmethod
    def is_blocked(user, partner):
        res = Match.objects.filter(user=user, partner=partner)
        if res.exists():
            match = Match.objects.get(user=user, partner=partner)
            if match.status == Match.Status.BLOCKED:
                return True
            else:
                return False
        else:
            return False

    @staticmethod
    def partner_matched(user, partner):
        res = Match.objects.filter(user=partner, partner=user)
        if res.exists():
            match = Match.objects.get(user=partner, partner=user)
            if match.status == Match.Status.MATCHED:
                return True
            else:
                return False
        else:
            return False

    @staticmethod
    def already_matched(user, partner):
        res = Match.objects.filter(user=user, partner=partner)
        if res.exists():
            match = Match.objects.get(user=user, partner=partner)
            if match.status == Match.Status.MATCHED:
                return True
            else:
                return False
        else:
            return False