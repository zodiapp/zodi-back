from django.db.models import QuerySet
from django.test import TestCase
from mock import mock, MagicMock
from django.http import JsonResponse, HttpRequest
from rest_framework import status
from rest_framework.utils import json

import matches.models
from matches.models import Match
from matches.views import MatchView


class MatchesUnitTests(TestCase):
    def test_is_blocked1(self):
        qs = mock.Mock(QuerySet)
        matches.models.Match.objects.filter = MagicMock(return_value=qs)
        qs.exists = MagicMock(return_value=True)
        match = mock.Mock(Match)
        match.status = Match.Status.BLOCKED
        matches.models.Match.objects.get = MagicMock(return_value=match)

        self.assertEqual(matches.models.Match.is_blocked(1, 2), True)

    def test_is_blocked2(self):
        qs = mock.Mock(QuerySet)
        matches.models.Match.objects.filter = MagicMock(return_value=qs)
        qs.exists = MagicMock(return_value=True)
        match = mock.Mock(Match)
        match.status = Match.Status.MATCHED
        matches.models.Match.objects.get = MagicMock(return_value=match)

        self.assertEqual(matches.models.Match.is_blocked(1, 2), False)

    def test_is_blocked3(self):
        qs = mock.Mock(QuerySet)
        matches.models.Match.objects.filter = MagicMock(return_value=qs)
        qs.exists = MagicMock(return_value=False)

        self.assertEqual(matches.models.Match.is_blocked(1, 2), False)

    def test_partner_matched(self):
        qs = mock.Mock(QuerySet)
        matches.models.Match.objects.filter = MagicMock(return_value=qs)
        qs.exists = MagicMock(return_value=True)
        match = mock.Mock(Match)
        match.status = Match.Status.BLOCKED
        matches.models.Match.objects.get = MagicMock(return_value=match)

        self.assertEqual(matches.models.Match.partner_matched(1, 2), False)

    def test_partner_matched2(self):
        qs = mock.Mock(QuerySet)
        matches.models.Match.objects.filter = MagicMock(return_value=qs)
        qs.exists = MagicMock(return_value=True)
        match = mock.Mock(Match)
        match.status = Match.Status.MATCHED
        matches.models.Match.objects.get = MagicMock(return_value=match)

        self.assertEqual(matches.models.Match.partner_matched(1, 2), True)

    def test_partner_matched3(self):
        qs = mock.Mock(QuerySet)
        matches.models.Match.objects.filter = MagicMock(return_value=qs)
        qs.exists = MagicMock(return_value=False)

        self.assertEqual(matches.models.Match.partner_matched(1, 2), False)


    def test_already_matched(self):
        qs = mock.Mock(QuerySet)
        matches.models.Match.objects.filter = MagicMock(return_value=qs)
        qs.exists = MagicMock(return_value=True)
        match = mock.Mock(Match)
        match.status = Match.Status.BLOCKED
        matches.models.Match.objects.get = MagicMock(return_value=match)

        self.assertEqual(matches.models.Match.already_matched(1, 2), False)

    def test_already_matched2(self):
        qs = mock.Mock(QuerySet)
        matches.models.Match.objects.filter = MagicMock(return_value=qs)
        qs.exists = MagicMock(return_value=True)
        match = mock.Mock(Match)
        match.status = Match.Status.MATCHED
        matches.models.Match.objects.get = MagicMock(return_value=match)

        self.assertEqual(matches.models.Match.already_matched(1, 2), True)

    def test_already_matched3(self):
        qs = mock.Mock(QuerySet)
        matches.models.Match.objects.filter = MagicMock(return_value=qs)
        qs.exists = MagicMock(return_value=False)

        self.assertEqual(matches.models.Match.already_matched(1, 2), False)

    def test_match(self):
        v = mock.Mock(spec=MatchView)
        v.body = MagicMock(return_value='t')
        v.body.decode = MagicMock(return_value="")
        matches.views.json.loads = MagicMock(return_value={'user1': 1, 'user2': 2})
        matches.views.get_object_or_404 = mock.MagicMock(return_value=1)
        Match.is_blocked = mock.MagicMock(return_value=True)
        res = MatchView.match(request=v)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.content, json.dumps({'status': "already_blocked"}).encode('utf-8'))

    def test_match2(self):
        v = mock.Mock(spec=MatchView)
        v.body = MagicMock(return_value='t')
        v.body.decode = MagicMock(return_value="")
        matches.views.json.loads = MagicMock(return_value={'user1': 1, 'user2': 2})
        matches.views.get_object_or_404 = mock.MagicMock(return_value=1)
        Match.is_blocked = mock.MagicMock(return_value=False)
        Match.already_matched = mock.MagicMock(return_value=True)
        res = MatchView.match(request=v)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.content, json.dumps({'status': "already_matched"}).encode('utf-8'))

    def test_block(self):
        v = mock.Mock(spec=MatchView)
        v.body = MagicMock(return_value='t')
        v.body.decode = MagicMock(return_value="")
        matches.views.json.loads = MagicMock(return_value={'user1': 1, 'user2': 2})
        matches.views.get_object_or_404 = mock.MagicMock(return_value=1)
        Match.is_blocked = mock.MagicMock(return_value=True)
        res = MatchView.block(request=v)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.content, json.dumps({'status': "already_blocked"}).encode('utf-8'))

    def test_block2(self):
        v = mock.Mock(spec=MatchView)
        v.body = MagicMock(return_value='t')
        v.body.decode = MagicMock(return_value="")
        matches.views.json.loads = MagicMock(return_value={'user1': 1, 'user2': 2})
        matches.views.get_object_or_404 = mock.MagicMock(return_value=1)
        Match.is_blocked = mock.MagicMock(return_value=False)
        Match.already_matched = mock.MagicMock(return_value=True)
        Match.objects.create = mock.MagicMock(return_value=mock.Mock(Match))
        Match.save = mock.MagicMock()
        res = MatchView.block(request=v)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(res.content, json.dumps({'status': "blocked"}).encode('utf-8'))