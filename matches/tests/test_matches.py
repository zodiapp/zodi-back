import json

from django.test import TestCase, Client, override_settings
from rest_framework import status

from users.models import ZodiUser


class MatchTests(TestCase):
    USERNAME = "test_user"
    USERNAME2 = "tu"
    PASSWORD = "some_pass"

    def setUp(self):
        self.user1 = ZodiUser.objects.create_user(username=self.USERNAME, password=self.PASSWORD)
        self.user2 = ZodiUser.objects.create_user(username=self.USERNAME2, password=self.PASSWORD)

    def test_match(self):
        with override_settings(
                CHANNEL_LAYERS={
                    "default": {
                        "BACKEND": "channels.layers.InMemoryChannelLayer"
                    }}):
            client = Client()
            resp = client.post('/matches/match/',  json.dumps({'user1': self.user1.id, 'user2': self.user2.id}),
                               content_type="application/json")
            self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
            self.assertEqual(json.loads(resp.content), {'status': 'match'})

            resp = client.post('/matches/match/',  json.dumps({'user1': self.user1.id, 'user2': self.user2.id}),
                               content_type="application/json")
            self.assertEqual(resp.status_code, status.HTTP_200_OK)
            self.assertEqual(json.loads(resp.content), {'status': 'already_matched'})

            resp = client.post('/matches/match/',  json.dumps({'user1': self.user2.id, 'user2': self.user1.id}),
                               content_type="application/json")
            self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
            self.assertEqual(json.loads(resp.content), {'status': 'matched'})

    def test_block(self):
        client = Client()
        resp = client.post('/matches/block/', json.dumps({'user1': self.user1.id, 'user2': self.user2.id}),
                           content_type="application/json")
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
        self.assertEqual(json.loads(resp.content), {'status': 'blocked'})

        resp = client.post('/matches/block/', json.dumps({'user1': self.user1.id, 'user2': self.user2.id}),
                           content_type="application/json")
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(resp.content), {'status': 'already_blocked'})

        resp = client.post('/matches/match/', json.dumps({'user1': self.user2.id, 'user2': self.user1.id}),
                           content_type="application/json")
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
        self.assertEqual(json.loads(resp.content), {'status': 'blocked'})

