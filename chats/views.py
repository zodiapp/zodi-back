from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404, get_list_or_404
from django.db.models import Q
from channels.db import database_sync_to_async

# Create your views here.
from chats.models import Message, Dialog
from users.models import ZodiUser


@login_required(login_url='/api/auth/')
def home(requests):
    return HttpResponse("Hello!")


def get_dialog(dialog_id):
    try:
        return get_object_or_404(Dialog, id=dialog_id)
    except Http404:
        return None


def get_messages(dialog_id):
    try:
        dialog = get_object_or_404(Dialog, id=dialog_id)
        return get_list_or_404(Message, dialog=dialog)
    except Http404:
        return []


def get_dialogs(user_id):
    try:
        user = get_object_or_404(ZodiUser, id=user_id)
        return get_list_or_404(Dialog, Q(user1=user) | Q(user2=user))
    except Http404:
        return []
