from django.db import models

# Create your models here.
from users.models import ZodiUser


class Dialog(models.Model):
    user1 = models.ForeignKey(ZodiUser, related_name='user1', on_delete=models.CASCADE)
    user2 = models.ForeignKey(ZodiUser, related_name='user2', on_delete=models.CASCADE)
    last_message = models.TextField(default="")

class Message(models.Model):
    dialog = models.ForeignKey(Dialog, on_delete=models.CASCADE)
    author = models.ForeignKey(ZodiUser, on_delete=models.CASCADE)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

