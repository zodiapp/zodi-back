from django.urls import re_path

from chats import consumers

websocket_urlpatterns = [
    re_path(r'ws/dialogs/(?P<user_id>\w+)/$', consumers.DialogConsumer.as_asgi()),
    re_path(r'ws/chat/(?P<dialog_id>\w+)/$', consumers.ChatConsumer.as_asgi()),
]