from django.urls import path

from chats import views

urlpatterns = [
    path('home', views.home, name='home')
]