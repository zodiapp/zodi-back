from sqlite3.dbapi2 import Timestamp

from django.test import TestCase

import chats.views
from chats.consumers import ChatConsumer, DialogConsumer
import mock

from chats.models import Message, Dialog
from users.models import ZodiUser
from chats.views import get_dialogs, get_dialog, get_messages


class ChatUnitTests(TestCase):
    def test_message_to_json(self):
        dialog = mock.Mock(spec=Dialog)
        dialog._state = mock.Mock()
        author = mock.Mock(spec=ZodiUser)
        author._state = mock.Mock()
        author._state.db = None
        timestamp = Timestamp(year=2021, month=1, day=1)
        content = "Hello"
        message = Message(id=1, dialog=dialog, author=author, timestamp=timestamp, content=content)
        consumer = ChatConsumer()
        res = consumer.message_to_json(message)
        self.assertEqual(res, {'id': 1, 'author': author.id, 'timestamp': '01/01/2021, 00:00:00', 'content': 'Hello'})

    def test_messages_to_json(self):
        dialog = mock.Mock(spec=Dialog)
        dialog._state = mock.Mock()
        author = mock.Mock(spec=ZodiUser)
        author._state = mock.Mock()
        author._state.db = None
        timestamp = Timestamp(year=2021, month=1, day=1)
        content = "Hello"
        message = Message(id=1, dialog=dialog, author=author, timestamp=timestamp, content=content)
        message2 = Message(id=2, dialog=dialog, author=author, timestamp=timestamp, content=content)
        consumer = ChatConsumer()
        messages = [message, message2]

        res = consumer.messages_to_json(messages)
        self.assertEqual(res, [{'id': 1, 'author': author.id, 'timestamp': '01/01/2021, 00:00:00', 'content': 'Hello'},
                               {'id':2, 'author': author.id, 'timestamp': '01/01/2021, 00:00:00', 'content': 'Hello'}])

    def test_dialogs_to_json(self):

        user1 = mock.Mock(spec=ZodiUser)
        user1._state = mock.Mock()
        user1._state.db = None

        user2 = mock.Mock(spec=ZodiUser)
        user2._state = mock.Mock()
        user2._state.db = None

        user3 = mock.Mock(spec=ZodiUser)
        user3._state = mock.Mock()
        user3._state.db = None

        dialog1 = Dialog(id=1, user1=user1, user2=user2, last_message="test")
        dialog2 = Dialog(id=2, user1=user1, user2=user3, last_message="test2")

        consumer = DialogConsumer()
        dialogs = [dialog1, dialog2]
        res = consumer.dialogs_to_json(dialogs=dialogs)
        self.assertEqual(res, [{'id': 1, 'last_message': 'test', 'user1': user1.id, 'user2': user2.id},
                               {'id': 2, 'last_message': 'test2', 'user1': user1.id, 'user2': user3.id}])

    def test_get_dialogs(self):

        chats.views.get_object_or_404 = mock.MagicMock(return_value=1)
        chats.views.get_list_or_404 = mock.MagicMock(return_value=[1, 2])

        self.assertEqual(get_dialogs(1), [1, 2])

    def test_get_dialog(self):
        chats.views.get_object_or_404 = mock.MagicMock(return_value=1)

        self.assertEqual(get_dialog(1), 1)

    def test_get_messages(self):
        chats.views.get_object_or_404 = mock.MagicMock(return_value=1)
        chats.views.get_list_or_404 = mock.MagicMock(return_value=[1, 2])

        self.assertEqual(get_messages(1), [1,2])