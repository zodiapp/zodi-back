import pytest
from channels.db import database_sync_to_async
from channels.routing import URLRouter
from channels.testing import WebsocketCommunicator
from django.conf.urls import url
from django.db.models import Q
from django.test import override_settings

from chats.consumers import ChatConsumer, DialogConsumer
from chats.models import Dialog, Message
from users.models import ZodiUser


@database_sync_to_async
def get_user_id(username):
    return ZodiUser.objects.filter(username=username).first().id


@database_sync_to_async
def get_dialog_id(user1, user2):
    return Dialog.objects.filter(
        Q(user1=user1, user2=user2) | Q(user1=user2, user2=user1)
    ).first().id


@database_sync_to_async
def get_message_count(dialog):
    return dialog.message_set.all().count()


@pytest.mark.django_db(transaction=True)
@pytest.mark.asyncio
async def test_chats():
    with override_settings(
            CHANNEL_LAYERS={
                "default": {
                    "BACKEND": "channels.layers.InMemoryChannelLayer"
                }}):

        FETCH_DIALOGS = 'fetch_dialogs'
        FETCH_MESSAGES = 'fetch_messages'
        NEW_MESSAGE = 'new_message'

        user1 = await database_sync_to_async(ZodiUser.objects.create_user)(username='Anna', password='123')
        user2 = await database_sync_to_async(ZodiUser.objects.create_user)(username='Mark', password='123')
        user3 = await database_sync_to_async(ZodiUser.objects.create_user)(username='Louis', password='123')
        dialog1 = await database_sync_to_async(Dialog.objects.create)(user1=user1, user2=user2, last_message="")
        dialog2 = await database_sync_to_async(Dialog.objects.create)(user1=user1, user2=user3, last_message="")
        dialog3 = await database_sync_to_async(Dialog.objects.create)(user1=user3, user2=user2, last_message="")
        message1 = await database_sync_to_async(
            Message.objects.create
        )(dialog=dialog1, author=user1, content='Hello 2!')
        message2 = await database_sync_to_async(
            Message.objects.create
        )(dialog=dialog1, author=user2, content='Hi 1!')
        message3 = await database_sync_to_async(
            Message.objects.create
        )(dialog=dialog2, author=user1, content='Hello 3!')
        message4 = await database_sync_to_async(
            Message.objects.create
        )(dialog=dialog3, author=user3, content='Hello 2!')

        app = URLRouter([url(r'ws/dialogs/(?P<user_id>\w+)/$', DialogConsumer.as_asgi())])
        communicator = WebsocketCommunicator(app, "/ws/dialogs/1/")
        connected, subprotocol = await communicator.connect()
        assert connected

        #Dialogs
        await communicator.send_json_to({
            'command': FETCH_DIALOGS,
            'user_id': await get_user_id('Anna')
        })
        response = await communicator.receive_json_from()
        assert response['command'] == 'dialogs'
        assert len(response['dialogs']) == 2

        await communicator.send_json_to({
            'command': FETCH_DIALOGS,
            'user_id': 10
        })
        response = await communicator.receive_json_from()
        assert response['command'] == 'dialogs'
        assert len(response['dialogs']) == 0
        await communicator.disconnect()



        app = URLRouter([url(r'ws/chat/(?P<dialog_id>\w+)/$', ChatConsumer.as_asgi())])
        communicator = WebsocketCommunicator(app, "/ws/chat/1/")
        connected, subprotocol = await communicator.connect()
        assert connected

        # Messages
        await communicator.send_json_to({
            'command': FETCH_MESSAGES,
            'dialog_id': await get_dialog_id(user1, user2)
        })
        messages = await communicator.receive_json_from()
        print(messages)
        assert messages['command'] == 'messages'
        assert len(messages['messages']) == 2

        await communicator.send_json_to({
            'command': FETCH_MESSAGES,
            'dialog_id': 10
        })
        messages = await communicator.receive_json_from()
        print(messages)
        assert messages['command'] == 'messages'
        assert len(messages['messages']) == 0

        # Send message
        await communicator.send_json_to({
            'command': NEW_MESSAGE,
            'dialog_id': await get_dialog_id(user1, user2),
            'user_id': await get_user_id('Anna'),
            'message': 'How are you?'
        })
        message = await communicator.receive_json_from()
        print(message)
        assert message['command'] == 'new_message'
        assert message['message']['content'] == 'How are you?'
        messages_in_chat1 = await get_message_count(dialog1)
        assert messages_in_chat1 == 3

        await communicator.send_json_to({
            'command': NEW_MESSAGE,
            'dialog_id': 10,
            'user_id': await get_user_id('Anna'),
            'message': 'How are you?'
        })
        message = await communicator.receive_json_from()
        print(message)
        assert message['command'] == 'new_message'
        assert message['message'] == {}

        await communicator.disconnect()
