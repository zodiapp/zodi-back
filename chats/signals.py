import channels.layers
from asgiref.sync import async_to_sync
from django.db.models.signals import post_save
from django.dispatch import receiver

from chats.models import Message, Dialog
from chats.views import get_dialog


@receiver(post_save, sender=Message)
def update_dialog_last_message(sender, instance, created, **kwargs):
    if not created:
        dialog = get_dialog(instance.dialog_id)
        dialog.last_message = instance.content
        dialog.save()


@receiver(post_save, sender=Dialog)
def new_message(sender, instance, **kwargs):
    dialog_name_1 = 'dialogs%s' % instance.user1.id
    dialog_name_2 = 'dialogs%s' % instance.user2.id
    message = {
        'id': instance.id,
        'user1': instance.user1.id,
        'user2': instance.user2.id,
        'last_message': instance.last_message
        }

    channel_layer = channels.layers.get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        dialog_name_1,
        {
            'type': 'update_dialog',
            'message': message
        }
    )
    async_to_sync(channel_layer.group_send)(
        dialog_name_2,
        {
            'type': 'update_dialog',
            'message': message
        }
    )