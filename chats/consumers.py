import json
from sqlite3.dbapi2 import Timestamp

import channels.layers
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from django.db.models.signals import post_save
from django.shortcuts import get_object_or_404
from django.dispatch import receiver
from users.models import ZodiUser
from .models import Message, Dialog
from .views import get_dialogs, get_messages, get_dialog


class ChatConsumer(WebsocketConsumer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.commands = {
            'fetch_messages': self.fetch_messages,
            'new_message': self.new_message
        }

    def messages_to_json(self, messages):
        result = []
        for message in messages:
            result.append(self.message_to_json(message))
        return result

    def message_to_json(self, message):
        return {
            'id': message.id,
            'author': message.author.id,
            'timestamp': message.timestamp.strftime('%m/%d/%Y, %H:%M:%S'),
            'content': message.content
        }

    def fetch_messages(self, data):
        dialog_id = data['dialog_id']
        messages = get_messages(dialog_id)
        content = {
            'command': 'messages',
            'messages': self.messages_to_json(messages)
        }
        self.send(text_data=json.dumps(content))

    def new_message(self, data):
        dialog = get_dialog(data['dialog_id'])
        user = get_object_or_404(ZodiUser, id=data['user_id'])
        if dialog is not None:
            message = Message.objects.create(dialog=dialog, author=user, content=data['message'], timestamp=Timestamp.now())
            message.save()
            content = {
                'command': 'new_message',
                'message': self.message_to_json(message)
            }
        else:
            content = {
                'command': 'new_message',
                'message': {}
            }
        return async_to_sync(self.channel_layer.group_send)(self.chat_name,
                                                            {
                                                                'type': 'chat_message',
                                                                'message': content
                                                            })

    def connect(self):
        self.chat_name = str('chat%s' % self.scope['url_route']['kwargs']['dialog_id'])
        async_to_sync(self.channel_layer.group_add)(self.chat_name, self.channel_name)
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(self.chat_name, self.channel_name)

    def receive(self, text_data=None, bytes_data=None):
        data = json.loads(text_data)
        self.commands[data['command']](data)

    def chat_message(self, event):
        message = event['message']
        self.send(text_data=json.dumps(message))


class DialogConsumer(WebsocketConsumer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.commands = {
            'fetch_dialogs': self.fetch_dialogs
        }

    def dialogs_to_json(self, dialogs):
        result = []
        for dialog in dialogs:
            result.append(
                {'id': dialog.id,
                 'user1': dialog.user1.id,
                 'user2': dialog.user2.id,
                 'last_message': dialog.last_message
                 })
        return result

    def fetch_dialogs(self, data):
        user_id = data['user_id']
        dialogs = get_dialogs(user_id)
        content = {
            'command': 'dialogs',
            'dialogs': self.dialogs_to_json(dialogs)
        }
        self.send(text_data=json.dumps(content))

    def connect(self):
        self.dialog_name = 'dialogs%s' % self.scope['url_route']['kwargs']['user_id']
        async_to_sync(self.channel_layer.group_add)(self.dialog_name, self.channel_name)
        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(self.dialog_name, self.channel_name)

    def receive(self, text_data=None, bytes_data=None):
        data = json.loads(text_data)
        self.commands[data['command']](data)

    def update_dialog(self, event):
        message = event['message']
        self.send(text_data=json.dumps(message))
