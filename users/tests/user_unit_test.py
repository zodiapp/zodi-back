from sqlite3.dbapi2 import Date

from django.test import TestCase

from users.models import ZodiUser


class UsersUnitTest(TestCase):
    def test_count_distance(self):
        user = ZodiUser()
        user.latitude = 15.0
        user.longitude = 25.0
        self.assertEqual(user.count_distance(10.0, 20.0), 777.1040313955309)

    def test_determine_sign(self):
        user = ZodiUser()
        self.assertEqual(user.determine_sign(), ZodiUser.Sign.SCORPIO)

    def test_determine_sign2(self):
        user = ZodiUser()
        user.birthdate = Date(year=2000, month=10, day=1)
        self.assertEqual(user.determine_sign(), ZodiUser.Sign.LIBRA)