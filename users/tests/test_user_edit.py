import json

from django.test import TestCase, RequestFactory
from django.urls import reverse

from users.models import ZodiUser


class AnimalTestCase(TestCase):
    USERNAME = "test_user"
    PASSWORD = "some_pass"

    def setUp(self):
        self.user1 = ZodiUser.objects.create_user(username=self.USERNAME, password=self.PASSWORD)

    def test_user_edit(self):
        self.client.login(username=self.USERNAME, password=self.PASSWORD)
        response = self.client.put(
            path=reverse('zodiuser-detail', args=[self.user1.id]),
            content_type='application/json',
            data={
                "birthdate": "1999-10-09T02:00:00",
                "preference_relationship": "RELATIONSHIP",
                "preference_gender": "FEMALE",
                "sex": "FEMALE"
            }
        )
        self.user1.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.user1.preference_gender, ZodiUser.PreferenceGender.FEMALE)
        self.assertEqual(self.user1.preference_relationship, ZodiUser.PreferenceRelationship.RELATIONSHIP)
        self.assertEqual(self.user1.birthdate.year, 1999)
        self.assertEqual(self.user1.sex, ZodiUser.Sex.WOMAN)

    def test_user_edit_non_auth(self):
        response = self.client.put(
            path=reverse('zodiuser-detail', args=[self.user1.id]),
            content_type='application/json',
            data={
                "birthdate": "1999-10-09T02:00:00",
                "preference_relationship": "RELATIONSHIP",
                "preference_gender": "FEMALE"
            }
        )
        self.assertEqual(response.status_code, 401)

    def test_user_edit_not_all_data(self):
        self.client.login(username=self.USERNAME, password=self.PASSWORD)
        response = self.client.put(
            path=reverse('zodiuser-detail', args=[self.user1.id]),
            content_type='application/json',
            data={
                "preference_relationship": "RELATIONSHIP",
                "preference_gender": "FEMALE"
            }
        )
        self.user1.refresh_from_db()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.user1.preference_gender, ZodiUser.PreferenceGender.FEMALE)
        self.assertEqual(self.user1.preference_relationship, ZodiUser.PreferenceRelationship.RELATIONSHIP)
        self.assertEqual(self.user1.birthdate, None)

    def test_user_edit_broken_data(self):
        self.client.login(username=self.USERNAME, password=self.PASSWORD)
        response = self.client.put(
            path=reverse('zodiuser-detail', args=[self.user1.id]),
            content_type='application/json',
            data={
                "birthdate": 228,
                "preference_relationship": "ASD",
                "preference_gender": "FEMALE"
            }
        )
        self.user1.refresh_from_db()
        self.assertEqual(response.status_code, 400)
        self.assertEqual(self.user1.preference_gender, None)
        self.assertEqual(self.user1.preference_relationship, None)
        self.assertEqual(self.user1.birthdate, None)
