from rest_framework import mixins
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from users.models import ZodiUser
from users.serializers import ZodiUserSerializer


class UpdateUserViewSet(mixins.UpdateModelMixin,
                        viewsets.GenericViewSet):

    serializer_class = ZodiUserSerializer
    permission_classes = [IsAuthenticated]
    queryset = ZodiUser.objects.all()
