from rest_framework import serializers

from users.models import ZodiUser


class ZodiUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = ZodiUser
        fields = ['birthdate', 'preference_relationship', 'preference_gender', 'sex']
