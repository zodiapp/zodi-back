from django.urls import path, include
from rest_framework import routers

from users.views import UpdateUserViewSet

router = routers.SimpleRouter()
router.register(r'', UpdateUserViewSet)

urlpatterns = [
    path('', include(router.urls)),
]
