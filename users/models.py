import math
from datetime import datetime
from math import radians, sin, atan2, sqrt, cos
from requests import request

from django.contrib.auth.models import AbstractUser
from django.db import models


class ZodiUser(AbstractUser):
    class PreferenceRelationship(models.TextChoices):
        FRIEND = 'FRIEND', 'Дружба'
        RELATIONSHIP = 'RELATIONSHIP', 'Отношения'
        SEX = 'SEX', 'Секс'

    class PreferenceGender(models.TextChoices):
        MALE = 'MALE', 'Мужчины'
        FEMALE = 'FEMALE', 'Женщины'
        BOTH = 'BOTH', 'Би'

    class Sex(models.TextChoices):
        MAN = "MALE", "Мужчины"
        WOMAN = "FEMALE", "Женщины"

    class Sign(models.TextChoices):
        ARIES = 'ARIES', 'Овен'
        TAURUS = 'TAURUS', 'Телец'
        GEMINI = 'GEMINI', 'Близнецы'
        CANCER = 'CANCER', 'Рак'
        LEO = 'LEO', 'Лев'
        VIRGO = 'VIRGO', 'Дева'
        LIBRA = 'LIBRA', 'Весы'
        SCORPIO = 'SCORPIO', 'Скорпион'
        SAGITTARIUS = 'SAGITTARIUS', 'Стрелец'
        CAPRICORN = 'CAPRICORN', 'Козерог'
        AQUARIUS = 'AQUARIUS', 'Водолей'
        PISCES = 'PISCES', 'Рыбы'

    main_image = models.ForeignKey('images.Image', on_delete=models.SET_NULL, related_name='users_main',
                                   null=True, blank=True)
    sex = models.CharField(max_length=255, choices=Sex.choices,
                           null=True, blank=True)
    images = models.ManyToManyField('images.Image', through='users.UserImage')
    birthdate = models.DateTimeField(null=True, blank=True)
    sign = models.CharField(max_length=255, choices=Sign.choices, null=True, blank=True)
    preference_relationship = models.CharField(max_length=255, choices=PreferenceRelationship.choices,
                                               null=True, blank=True)
    preference_zodiac = models.JSONField(null=True, blank=True)
    preference_gender = models.CharField(max_length=255, choices=PreferenceGender.choices,
                                         null=True, blank=True)
    latitude = models.FloatField(null=True, blank=True)
    longitude = models.FloatField(null=True, blank=True)

    def save(self, *args, **kwargs):
        self.sign = self.determine_sign()
        super(ZodiUser, self).save(*args, **kwargs)

    def type_math(self, other_date):
        date_1 = other_date.date().strftime("%Y-%m-%d")
        name_1 = "name1"
        date_2 = self.birthdate.date().strftime("%Y-%m-%d")
        name_2 = "name2"

        querystring = {"mystic_dob": date_1, "mystic_dob2": date_2, "mystic_name": name_1, "mystic_name2": name_2}

        headers = {
            'content-type': "application/x-www-form-urlencoded",
            'x-rapidapi-key': "29cc7723cemsh7bf8f4b70cb12f6p1ef2b8jsnb25463663024",
            'x-rapidapi-host': "astrology-horoscope.p.rapidapi.com"
        }

        response = request("POST", "https://astrology-horoscope.p.rapidapi.com/zodiac_compatibility/result/",
                           headers=headers, params=querystring)
        if response.status_code != 200:
            return "Something magic"
        match_result = response.json()["data"]["result"]["Compatibility"]["Heading"]

        return match_result

    def count_distance(self, other_lat, other_long):
        R = 6373.0

        if other_lat is None or other_long is None:
            return 0

        lat1 = radians(self.latitude)
        lon1 = radians(self.longitude)
        lat2 = radians(other_lat)
        lon2 = radians(other_long)

        dlon = lon2 - lon1
        dlat = lat2 - lat1

        a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
        c = 2 * atan2(sqrt(a), sqrt(1 - a))

        distance = R * c
        return distance

    def determine_sign(self):
        if not self.birthdate:
            return self.Sign.SCORPIO
        date = self.birthdate
        month = date.month
        day = date.day
        if month == 12:
            astro_sign = self.Sign.SAGITTARIUS if (day < 22) else self.Sign.CAPRICORN
        elif month == 1:
            astro_sign = self.Sign.CAPRICORN if (day < 20) else self.Sign.AQUARIUS
        elif month == 2:
            astro_sign = self.Sign.AQUARIUS if (day < 19) else self.Sign.PISCES
        elif month == 3:
            astro_sign = self.Sign.PISCES if (day < 21) else self.Sign.ARIES
        elif month == 4:
            astro_sign = self.Sign.ARIES if (day < 20) else self.Sign.TAURUS
        elif month == 5:
            astro_sign = self.Sign.TAURUS if (day < 21) else self.Sign.GEMINI
        elif month == 6:
            astro_sign = self.Sign.GEMINI if (day < 21) else self.Sign.CANCER
        elif month == 7:
            astro_sign = self.Sign.CANCER if (day < 23) else self.Sign.LEO
        elif month == 8:
            astro_sign = self.Sign.LEO if (day < 23) else self.Sign.VIRGO
        elif month == 9:
            astro_sign = self.Sign.VIRGO if (day < 23) else self.Sign.LIBRA
        elif month == 10:
            astro_sign = self.Sign.LIBRA if (day < 23) else self.Sign.SCORPIO
        elif month == 11:
            astro_sign = self.Sign.SCORPIO if (day < 22) else self.Sign.SAGITTARIUS
        else:
            astro_sign = None

        return astro_sign


class UserImage(models.Model):
    user = models.ForeignKey(ZodiUser, on_delete=models.CASCADE)
    image = models.ForeignKey('images.Image', on_delete=models.CASCADE)
    private = models.BooleanField(default=False)
