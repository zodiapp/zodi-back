[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=zodiapp_zodi-back&metric=alert_status&token=c1557a099a26aa04fc0835e38ee223a346db4fda)](https://sonarcloud.io/dashboard?id=zodiapp_zodi-back)
[![Python version](https://img.shields.io/badge/Python-3.9-blue.svg)](https://shields.io/)
[![Django version](https://img.shields.io/badge/Django-3.1.6-darkgreen.svg)](https://shields.io/)

[Sonar dashboard](https://sonarcloud.io/dashboard?id=zodiapp_zodi-back)
